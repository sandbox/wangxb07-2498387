/**
 * @file
 * weixin_platform javascript
 */

(function ($) {

  Drupal.WP = {};

  Drupal.behaviors.WP = {
    attach: function(context, settings) {
    }
  };

  Drupal.WP.invokeAPI = function(settings, apiNames, callback) {
    wx.config({
      debug: settings.weixinPlatform.debug, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
      appId: settings.weixinPlatform.appid, // 必填，公众号的唯一标识
      timestamp: settings.weixinPlatform.timestamp, // 必填，生成签名的时间戳
      nonceStr: settings.weixinPlatform.nonceStr, // 必填，生成签名的随机串
      signature: settings.weixinPlatform.signature,// 必填，签名，见附录1
      jsApiList: apiNames // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
    });

    wx.ready(function(){
      wx.checkJsApi({
        jsApiList: apiNames, // 需要检测的JS接口列表，所有JS接口列表见附录2,
        success: function(res) {
          callback(settings);
        }
      });
    });

    wx.error(function(res) {
    });
  }

})(jQuery, wx);
