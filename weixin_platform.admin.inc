<?php
/**
 * @file
 */

/**
 * Settings form callback
 * @ingroup forms
 */
function weixin_platform_settings_form() {
  $form = array();

  $form['weixin_platform_appid'] = array(
    '#type' => 'textfield',
    '#title' => t('APPID'),
    '#description' => t('weixin platform app id'),
    '#required' => TRUE,
    '#default_value' => variable_get('weixin_platform_appid', ''),
  );
  $form['weixin_platform_app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('APP Secret'),
    '#description' => t('weinxin platform app secret string'),
    '#required' => TRUE,
    '#default_value' => variable_get('weixin_platform_app_secret', ''),
  );
  $form['weixin_platform_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Token'),
    '#description' => t('weinxin platform validate token'),
    '#required' => TRUE,
    '#default_value' => variable_get('weixin_platform_token', ''),
  );
  $form['weixin_platform_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#description' => t('weinxin platform debug mode'),
    '#required' => FALSE,
    '#default_value' => variable_get('weixin_platform_debug', FALSE),
  );

  return system_settings_form($form);
}

/**
 * weixin platform events table
 */
function weixin_platform_events() {
  $content = array();

  ctools_include('plugins');
  $plugins = ctools_get_plugins('weixin_platform', 'weixin_platform_event_handler');

  $header = array(
    'name' => array('data' => 'Name'),
    'module_name' => array('data' => 'Module Name'),
    'description' => array('data' => 'Description'),
  );

  $rows = array();
  foreach ($plugins as $i => $plugin) {
    $rows[$i] = array(
      'name' => $plugin['name'],
      'module_name' => $plugin['module'],
      'description' => $plugin['description'],
    );
  }

  $content['plugins'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No event handler available.'),
  );

  return $content;
}
